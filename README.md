GENERAL INFORMATION

Title of Dataset: NOWA Choice-RT Task

Author/Principal Investigator Information
Name: Philipp Flieger
Institution: Justus-Liebig-Universität Gießen
Address: Arndstraße 2, 35392 Gießen
Email: Philipp.Flieger@math.uni-giessen.de

Author/Associate Information
Name: Micha Engeser
Institution: Justus-Liebig-Universität Gießen
Address: Arndstraße 2, 35392 Gießen
Email: Micha.Engeser@math.uni-giessen.de

Date of data collection: 2024-02-12 - 2024-02-16

SHARING/ACCESS IMFORMATION
Licenses/restrictions placed on the data: MIT License
OSF: https://osf.io/swkjp/

DATA & FILE OVERVIEW
Folder list: Data, Experiment code, Preprint

METHODOLOGICAL INFORMATION
Experiment based on: https://doi.org/10.3758/s13428-010-0024-1 (cf. https://gitlab.pavlovia.org/demos/choicertt)

[...]
