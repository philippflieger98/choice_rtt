﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v2023.2.3),
    on Wed Feb 14 12:23:37 2024
If you publish work using this script the most relevant publication is:

    Peirce J, Gray JR, Simpson S, MacAskill M, Höchenberger R, Sogo H, Kastman E, Lindeløv JK. (2019) 
        PsychoPy2: Experiments in behavior made easy Behav Res 51: 195. 
        https://doi.org/10.3758/s13428-018-01193-y

"""

import psychopy
psychopy.useVersion('2023.2.3')


# --- Import packages ---
from psychopy import locale_setup
from psychopy import prefs
from psychopy import plugins
plugins.activatePlugins()
prefs.hardware['audioLib'] = 'ptb'
prefs.hardware['audioLatencyMode'] = '3'
from psychopy import sound, gui, visual, core, data, event, logging, clock, colors, layout
from psychopy.tools import environmenttools
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER, priority)

import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle, choice as randchoice
import os  # handy system and path functions
import sys  # to get file system encoding

from psychopy.hardware import keyboard

# --- Setup global variables (available in all functions) ---
# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
# Store info about the experiment session
psychopyVersion = '2023.2.3'
expName = 'choice_rtt'  # from the Builder filename that created this script
expInfo = {
    'participant': '',
    'session': '',
    'age': '',
    'left-handed': False,
    'Do you like this session?': ['Yes', 'No'],
    'output-path': '',
    'date': data.getDateStr(),  # add a simple timestamp
    'expName': expName,
    'psychopyVersion': psychopyVersion,
}


def showExpInfoDlg(expInfo):
    """
    Show participant info dialog.
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    
    Returns
    ==========
    dict
        Information about this experiment.
    """
    # temporarily remove keys which the dialog doesn't need to show
    poppedKeys = {
        'date': expInfo.pop('date', data.getDateStr()),
        'expName': expInfo.pop('expName', expName),
        'psychopyVersion': expInfo.pop('psychopyVersion', psychopyVersion),
    }
    # show participant info dialog
    dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
    if dlg.OK == False:
        core.quit()  # user pressed cancel
    # restore hidden keys
    expInfo.update(poppedKeys)
    # return expInfo
    return expInfo


def setupData(expInfo, dataDir=None):
    """
    Make an ExperimentHandler to handle trials and saving.
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    dataDir : Path, str or None
        Folder to save the data to, leave as None to create a folder in the current directory.    
    Returns
    ==========
    psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    """
    
    # data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
    if dataDir is None:
        dataDir = _thisDir
    filename = u'%s/sub-%s/ses-%s/%s_%s_%s_%s' % (expInfo['output-path'], expInfo['participant'], expInfo['session'], expInfo['participant'], expInfo['session'], expName, expInfo['date'])
    # make sure filename is relative to dataDir
    if os.path.isabs(filename):
        dataDir = os.path.commonprefix([dataDir, filename])
        filename = os.path.relpath(filename, dataDir)
    
    # an ExperimentHandler isn't essential but helps with data saving
    thisExp = data.ExperimentHandler(
        name=expName, version='',
        extraInfo=expInfo, runtimeInfo=None,
        originPath='/Users/philippflieger/PhD/NOWA/choice_rtt/code/experiment/choice_rtt_lastrun.py',
        savePickle=True, saveWideText=False,
        dataFileName=dataDir + os.sep + filename, sortColumns='time'
    )
    thisExp.setPriority('thisRow.t', priority.CRITICAL)
    thisExp.setPriority('expName', priority.LOW)
    # return experiment handler
    return thisExp


def setupLogging(filename):
    """
    Setup a log file and tell it what level to log at.
    
    Parameters
    ==========
    filename : str or pathlib.Path
        Filename to save log file and data files as, doesn't need an extension.
    
    Returns
    ==========
    psychopy.logging.LogFile
        Text stream to receive inputs from the logging system.
    """
    # this outputs to the screen, not a file
    logging.console.setLevel(logging.EXP)
    # save a log file for detail verbose info
    logFile = logging.LogFile(filename+'.log', level=logging.EXP)
    
    return logFile


def setupWindow(expInfo=None, win=None):
    """
    Setup the Window
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    win : psychopy.visual.Window
        Window to setup - leave as None to create a new window.
    
    Returns
    ==========
    psychopy.visual.Window
        Window in which to run this experiment.
    """
    if win is None:
        # if not given a window to setup, make one
        win = visual.Window(
            size=[1512, 982], fullscr=True, screen=0,
            winType='pyglet', allowStencil=False,
            monitor='testMonitor', color=[0,0,0], colorSpace='rgb',
            backgroundImage='', backgroundFit='none',
            blendMode='avg', useFBO=True,
            units='height'
        )
        if expInfo is not None:
            # store frame rate of monitor if we can measure it
            expInfo['frameRate'] = win.getActualFrameRate()
    else:
        # if we have a window, just set the attributes which are safe to set
        win.color = [0,0,0]
        win.colorSpace = 'rgb'
        win.backgroundImage = ''
        win.backgroundFit = 'none'
        win.units = 'height'
    win.mouseVisible = False
    win.hideMessage()
    return win


def setupInputs(expInfo, thisExp, win):
    """
    Setup whatever inputs are available (mouse, keyboard, eyetracker, etc.)
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    win : psychopy.visual.Window
        Window in which to run this experiment.
    Returns
    ==========
    dict
        Dictionary of input devices by name.
    """
    # --- Setup input devices ---
    inputs = {}
    ioConfig = {}
    ioSession = ioServer = eyetracker = None
    
    # create a default keyboard (e.g. to check for escape)
    defaultKeyboard = keyboard.Keyboard(backend='ptb')
    # return inputs dict
    return {
        'ioServer': ioServer,
        'defaultKeyboard': defaultKeyboard,
        'eyetracker': eyetracker,
    }

def pauseExperiment(thisExp, inputs=None, win=None, timers=[], playbackComponents=[]):
    """
    Pause this experiment, preventing the flow from advancing to the next routine until resumed.
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    inputs : dict
        Dictionary of input devices by name.
    win : psychopy.visual.Window
        Window for this experiment.
    timers : list, tuple
        List of timers to reset once pausing is finished.
    playbackComponents : list, tuple
        List of any components with a `pause` method which need to be paused.
    """
    # if we are not paused, do nothing
    if thisExp.status != PAUSED:
        return
    
    # pause any playback components
    for comp in playbackComponents:
        comp.pause()
    # prevent components from auto-drawing
    win.stashAutoDraw()
    # run a while loop while we wait to unpause
    while thisExp.status == PAUSED:
        # make sure we have a keyboard
        if inputs is None:
            inputs = {
                'defaultKeyboard': keyboard.Keyboard(backend='PsychToolbox')
            }
        # check for quit (typically the Esc key)
        if inputs['defaultKeyboard'].getKeys(keyList=['escape']):
            endExperiment(thisExp, win=win, inputs=inputs)
        # flip the screen
        win.flip()
    # if stop was requested while paused, quit
    if thisExp.status == FINISHED:
        endExperiment(thisExp, inputs=inputs, win=win)
    # resume any playback components
    for comp in playbackComponents:
        comp.play()
    # restore auto-drawn components
    win.retrieveAutoDraw()
    # reset any timers
    for timer in timers:
        timer.reset()


def run(expInfo, thisExp, win, inputs, globalClock=None, thisSession=None):
    """
    Run the experiment flow.
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    psychopy.visual.Window
        Window in which to run this experiment.
    inputs : dict
        Dictionary of input devices by name.
    globalClock : psychopy.core.clock.Clock or None
        Clock to get global time from - supply None to make a new one.
    thisSession : psychopy.session.Session or None
        Handle of the Session object this experiment is being run from, if any.
    """
    # mark experiment as started
    thisExp.status = STARTED
    # make sure variables created by exec are available globally
    exec = environmenttools.setExecEnvironment(globals())
    # get device handles from dict of input devices
    ioServer = inputs['ioServer']
    defaultKeyboard = inputs['defaultKeyboard']
    eyetracker = inputs['eyetracker']
    # make sure we're running in the directory for this experiment
    os.chdir(_thisDir)
    # get filename from ExperimentHandler for convenience
    filename = thisExp.dataFileName
    frameTolerance = 0.001  # how close to onset before 'same' frame
    endExpNow = False  # flag for 'escape' or other condition => quit the exp
    # get frame duration from frame rate in expInfo
    if 'frameRate' in expInfo and expInfo['frameRate'] is not None:
        frameDur = 1.0 / round(expInfo['frameRate'])
    else:
        frameDur = 1.0 / 60.0  # could not measure, so guess
    
    # Start Code - component code to be run after the window creation
    
    # --- Initialize components for Routine "begin_study" ---
    welcome_msg = visual.TextStim(win=win, name='welcome_msg',
        text='Welcome to the study!\n\nPress [Space] to continue...',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=-1.0);
    space_welcome = keyboard.Keyboard()
    
    # --- Initialize components for Routine "instructions" ---
    instructions_msg = visual.TextStim(win=win, name='instructions_msg',
        text='In this task, you will make decisions as to which stimulus you have seen. There are three versions of the task. In the first, you have to decide which shape you have seen, in the second which image and the third which sound you’ve heard. Before each task, you will get set of specific instructions and short practice period. \n\nPlease press the space bar to continue.',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    space_instr = keyboard.Keyboard()
    
    # --- Initialize components for Routine "crtt_instr" ---
    crtt_instr_msg = visual.TextStim(win=win, name='crtt_instr_msg',
        text='In this task you will make a decision as to which shape you have seen.\n\nPress C or click cross for a cross Press V or click square for a square Press B or click plus for a plus\n\nFirst, we will have a quick practice.\n\nPush space bar or click / touch one of the buttons to begin.',
        font='Open Sans',
        pos=(0, 0.1), height=0.035, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    crtt_space = keyboard.Keyboard()
    response_square = visual.ImageStim(
        win=win,
        name='response_square', 
        image='/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/response_square.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-2.0)
    response_plus = visual.ImageStim(
        win=win,
        name='response_plus', 
        image='/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/response_plus.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-3.0)
    response_cross = visual.ImageStim(
        win=win,
        name='response_cross', 
        image='/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/response_cross.jpg', mask=None, anchor='center',
        ori=0.0, pos=(-0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-4.0)
    
    # --- Initialize components for Routine "trial" ---
    practice_trial_square = visual.ImageStim(
        win=win,
        name='practice_trial_square', 
        image='/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/response_square.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-2.0)
    practice_trial_plus = visual.ImageStim(
        win=win,
        name='practice_trial_plus', 
        image='/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/response_plus.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-3.0)
    practice_trial_cross = visual.ImageStim(
        win=win,
        name='practice_trial_cross', 
        image='/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/response_cross.jpg', mask=None, anchor='center',
        ori=0.0, pos=(-0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-4.0)
    left_far_tile = visual.ImageStim(
        win=win,
        name='left_far_tile', 
        image='/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-5.0)
    left_mid_tile = visual.ImageStim(
        win=win,
        name='left_mid_tile', 
        image='/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-6.0)
    right_mid_tile = visual.ImageStim(
        win=win,
        name='right_mid_tile', 
        image='/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-7.0)
    right_far_tile = visual.ImageStim(
        win=win,
        name='right_far_tile', 
        image='/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(0.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-8.0)
    target_image = visual.ImageStim(
        win=win,
        name='target_image', 
        image='default.png', mask=None, anchor='center',
        ori=0.0, pos=[0,0], size=(0.2, 0.2),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-9.0)
    keyboard_response = keyboard.Keyboard()
    
    # --- Initialize components for Routine "practice_feedback" ---
    feedback_text = visual.TextStim(win=win, name='feedback_text',
        text='',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    next_trial = keyboard.Keyboard()
    
    # --- Initialize components for Routine "instructions_shape_exp" ---
    crtt_instr_msg_2 = visual.TextStim(win=win, name='crtt_instr_msg_2',
        text='Well done, you have finished the practice! Now for the main experiment. \n\nPress space to continue!',
        font='Open Sans',
        pos=(0, 0.1), height=0.035, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    crtt_space_2 = keyboard.Keyboard()
    
    # --- Initialize components for Routine "trial" ---
    practice_trial_square = visual.ImageStim(
        win=win,
        name='practice_trial_square', 
        image='/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/response_square.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-2.0)
    practice_trial_plus = visual.ImageStim(
        win=win,
        name='practice_trial_plus', 
        image='/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/response_plus.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-3.0)
    practice_trial_cross = visual.ImageStim(
        win=win,
        name='practice_trial_cross', 
        image='/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/response_cross.jpg', mask=None, anchor='center',
        ori=0.0, pos=(-0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-4.0)
    left_far_tile = visual.ImageStim(
        win=win,
        name='left_far_tile', 
        image='/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-5.0)
    left_mid_tile = visual.ImageStim(
        win=win,
        name='left_mid_tile', 
        image='/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-6.0)
    right_mid_tile = visual.ImageStim(
        win=win,
        name='right_mid_tile', 
        image='/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-7.0)
    right_far_tile = visual.ImageStim(
        win=win,
        name='right_far_tile', 
        image='/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(0.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-8.0)
    target_image = visual.ImageStim(
        win=win,
        name='target_image', 
        image='default.png', mask=None, anchor='center',
        ori=0.0, pos=[0,0], size=(0.2, 0.2),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-9.0)
    keyboard_response = keyboard.Keyboard()
    
    # --- Initialize components for Routine "end_screen" ---
    end_screen_msg = visual.TextStim(win=win, name='end_screen_msg',
        text='The end!',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    
    # create some handy timers
    if globalClock is None:
        globalClock = core.Clock()  # to track the time since experiment started
    if ioServer is not None:
        ioServer.syncClock(globalClock)
    logging.setDefaultClock(globalClock)
    routineTimer = core.Clock()  # to track time remaining of each (possibly non-slip) routine
    win.flip()  # flip window to reset last flip timer
    # store the exact time the global clock started
    expInfo['expStart'] = data.getDateStr(format='%Y-%m-%d %Hh%M.%S.%f %z', fractionalSecondDigits=6)
    
    # --- Prepare to start Routine "begin_study" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('begin_study.started', globalClock.getTime())
    # Run 'Begin Routine' code from create_dirs
    # Construct the path with placeholders replaced by actual values
    dir_path = expInfo['output-path'] + "/sub-" + expInfo['participant'] + "/ses-" + expInfo['session']
    
    # Check if the path exists
    if not os.path.exists(dir_path):
        # Create the directory, including all intermediate directories
        os.makedirs(path)
        print(f"Directory '{dir_path}' was created.")
    else:
        print(f"Directory '{dir_path}' already exists.")
    space_welcome.keys = []
    space_welcome.rt = []
    _space_welcome_allKeys = []
    # keep track of which components have finished
    begin_studyComponents = [welcome_msg, space_welcome]
    for thisComponent in begin_studyComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "begin_study" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *welcome_msg* updates
        
        # if welcome_msg is starting this frame...
        if welcome_msg.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            welcome_msg.frameNStart = frameN  # exact frame index
            welcome_msg.tStart = t  # local t and not account for scr refresh
            welcome_msg.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(welcome_msg, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'welcome_msg.started')
            # update status
            welcome_msg.status = STARTED
            welcome_msg.setAutoDraw(True)
        
        # if welcome_msg is active this frame...
        if welcome_msg.status == STARTED:
            # update params
            pass
        
        # *space_welcome* updates
        waitOnFlip = False
        
        # if space_welcome is starting this frame...
        if space_welcome.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            space_welcome.frameNStart = frameN  # exact frame index
            space_welcome.tStart = t  # local t and not account for scr refresh
            space_welcome.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(space_welcome, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'space_welcome.started')
            # update status
            space_welcome.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(space_welcome.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(space_welcome.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if space_welcome.status == STARTED and not waitOnFlip:
            theseKeys = space_welcome.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _space_welcome_allKeys.extend(theseKeys)
            if len(_space_welcome_allKeys):
                space_welcome.keys = _space_welcome_allKeys[-1].name  # just the last key pressed
                space_welcome.rt = _space_welcome_allKeys[-1].rt
                space_welcome.duration = _space_welcome_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in begin_studyComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "begin_study" ---
    for thisComponent in begin_studyComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('begin_study.stopped', globalClock.getTime())
    # check responses
    if space_welcome.keys in ['', [], None]:  # No response was made
        space_welcome.keys = None
    thisExp.addData('space_welcome.keys',space_welcome.keys)
    if space_welcome.keys != None:  # we had a response
        thisExp.addData('space_welcome.rt', space_welcome.rt)
        thisExp.addData('space_welcome.duration', space_welcome.duration)
    thisExp.nextEntry()
    # the Routine "begin_study" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # --- Prepare to start Routine "instructions" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('instructions.started', globalClock.getTime())
    space_instr.keys = []
    space_instr.rt = []
    _space_instr_allKeys = []
    # keep track of which components have finished
    instructionsComponents = [instructions_msg, space_instr]
    for thisComponent in instructionsComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "instructions" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *instructions_msg* updates
        
        # if instructions_msg is starting this frame...
        if instructions_msg.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            instructions_msg.frameNStart = frameN  # exact frame index
            instructions_msg.tStart = t  # local t and not account for scr refresh
            instructions_msg.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(instructions_msg, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'instructions_msg.started')
            # update status
            instructions_msg.status = STARTED
            instructions_msg.setAutoDraw(True)
        
        # if instructions_msg is active this frame...
        if instructions_msg.status == STARTED:
            # update params
            pass
        
        # *space_instr* updates
        waitOnFlip = False
        
        # if space_instr is starting this frame...
        if space_instr.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            space_instr.frameNStart = frameN  # exact frame index
            space_instr.tStart = t  # local t and not account for scr refresh
            space_instr.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(space_instr, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'space_instr.started')
            # update status
            space_instr.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(space_instr.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(space_instr.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if space_instr.status == STARTED and not waitOnFlip:
            theseKeys = space_instr.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _space_instr_allKeys.extend(theseKeys)
            if len(_space_instr_allKeys):
                space_instr.keys = _space_instr_allKeys[-1].name  # just the last key pressed
                space_instr.rt = _space_instr_allKeys[-1].rt
                space_instr.duration = _space_instr_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in instructionsComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "instructions" ---
    for thisComponent in instructionsComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('instructions.stopped', globalClock.getTime())
    # check responses
    if space_instr.keys in ['', [], None]:  # No response was made
        space_instr.keys = None
    thisExp.addData('space_instr.keys',space_instr.keys)
    if space_instr.keys != None:  # we had a response
        thisExp.addData('space_instr.rt', space_instr.rt)
        thisExp.addData('space_instr.duration', space_instr.duration)
    thisExp.nextEntry()
    # the Routine "instructions" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # --- Prepare to start Routine "crtt_instr" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('crtt_instr.started', globalClock.getTime())
    crtt_space.keys = []
    crtt_space.rt = []
    _crtt_space_allKeys = []
    # keep track of which components have finished
    crtt_instrComponents = [crtt_instr_msg, crtt_space, response_square, response_plus, response_cross]
    for thisComponent in crtt_instrComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "crtt_instr" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *crtt_instr_msg* updates
        
        # if crtt_instr_msg is starting this frame...
        if crtt_instr_msg.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            crtt_instr_msg.frameNStart = frameN  # exact frame index
            crtt_instr_msg.tStart = t  # local t and not account for scr refresh
            crtt_instr_msg.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(crtt_instr_msg, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'crtt_instr_msg.started')
            # update status
            crtt_instr_msg.status = STARTED
            crtt_instr_msg.setAutoDraw(True)
        
        # if crtt_instr_msg is active this frame...
        if crtt_instr_msg.status == STARTED:
            # update params
            pass
        
        # *crtt_space* updates
        waitOnFlip = False
        
        # if crtt_space is starting this frame...
        if crtt_space.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            crtt_space.frameNStart = frameN  # exact frame index
            crtt_space.tStart = t  # local t and not account for scr refresh
            crtt_space.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(crtt_space, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'crtt_space.started')
            # update status
            crtt_space.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(crtt_space.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(crtt_space.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if crtt_space.status == STARTED and not waitOnFlip:
            theseKeys = crtt_space.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _crtt_space_allKeys.extend(theseKeys)
            if len(_crtt_space_allKeys):
                crtt_space.keys = _crtt_space_allKeys[-1].name  # just the last key pressed
                crtt_space.rt = _crtt_space_allKeys[-1].rt
                crtt_space.duration = _crtt_space_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # *response_square* updates
        
        # if response_square is starting this frame...
        if response_square.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            response_square.frameNStart = frameN  # exact frame index
            response_square.tStart = t  # local t and not account for scr refresh
            response_square.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(response_square, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'response_square.started')
            # update status
            response_square.status = STARTED
            response_square.setAutoDraw(True)
        
        # if response_square is active this frame...
        if response_square.status == STARTED:
            # update params
            pass
        
        # *response_plus* updates
        
        # if response_plus is starting this frame...
        if response_plus.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            response_plus.frameNStart = frameN  # exact frame index
            response_plus.tStart = t  # local t and not account for scr refresh
            response_plus.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(response_plus, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'response_plus.started')
            # update status
            response_plus.status = STARTED
            response_plus.setAutoDraw(True)
        
        # if response_plus is active this frame...
        if response_plus.status == STARTED:
            # update params
            pass
        
        # *response_cross* updates
        
        # if response_cross is starting this frame...
        if response_cross.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            response_cross.frameNStart = frameN  # exact frame index
            response_cross.tStart = t  # local t and not account for scr refresh
            response_cross.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(response_cross, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'response_cross.started')
            # update status
            response_cross.status = STARTED
            response_cross.setAutoDraw(True)
        
        # if response_cross is active this frame...
        if response_cross.status == STARTED:
            # update params
            pass
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in crtt_instrComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "crtt_instr" ---
    for thisComponent in crtt_instrComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('crtt_instr.stopped', globalClock.getTime())
    # check responses
    if crtt_space.keys in ['', [], None]:  # No response was made
        crtt_space.keys = None
    thisExp.addData('crtt_space.keys',crtt_space.keys)
    if crtt_space.keys != None:  # we had a response
        thisExp.addData('crtt_space.rt', crtt_space.rt)
        thisExp.addData('crtt_space.duration', crtt_space.duration)
    thisExp.nextEntry()
    # the Routine "crtt_instr" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # set up handler to look after randomisation of conditions etc
    PracticeLoop = data.TrialHandler(nReps=1.0, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=data.importConditions('/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/shapes_targets.csv'),
        seed=None, name='PracticeLoop')
    thisExp.addLoop(PracticeLoop)  # add the loop to the experiment
    thisPracticeLoop = PracticeLoop.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisPracticeLoop.rgb)
    if thisPracticeLoop != None:
        for paramName in thisPracticeLoop:
            globals()[paramName] = thisPracticeLoop[paramName]
    
    for thisPracticeLoop in PracticeLoop:
        currentLoop = PracticeLoop
        thisExp.timestampOnFlip(win, 'thisRow.t')
        # pause experiment here if requested
        if thisExp.status == PAUSED:
            pauseExperiment(
                thisExp=thisExp, 
                inputs=inputs, 
                win=win, 
                timers=[routineTimer], 
                playbackComponents=[]
        )
        # abbreviate parameter names if possible (e.g. rgb = thisPracticeLoop.rgb)
        if thisPracticeLoop != None:
            for paramName in thisPracticeLoop:
                globals()[paramName] = thisPracticeLoop[paramName]
        
        # --- Prepare to start Routine "trial" ---
        continueRoutine = True
        # update component parameters for each repeat
        thisExp.addData('trial.started', globalClock.getTime())
        # Run 'Begin Routine' code from jitter_onset_loc
        #list of possible onsets for target
        list_onsets = [1, 1.2, 1.4, 1.6, 1.8]
        
        # randomize these onsets
        shuffle(list_onsets)
        
        #pick the first value from the list 
        onset_trial = list_onsets[0]
        
        #list of possible positions for target
        list_positions = [-0.375, -0.125, 0.125, 0.375]
        
        # randomize these onsets
        shuffle(list_positions)
        
        #pick the first value from the list 
        position_trial = list_positions[0]
        
        #TargetImage is the variable in the Image field of the target_image component
        
        #path of where to find the target image
        if TargetImage == '../../stimuli/shapes/target_square.jpg': #path of where to find the target image
            
            #setting the key press that will be the correct answer
            corrAns = 'v' 
            
        #path of where to find the target image
        elif TargetImage == '../../stimuli/shapes/target_cross.jpg':
        
            #setting the key press that will be the correct answer
            corrAns = 'c'
        
        #path of where to find the target image
        elif TargetImage == '../../stimuli/shapes/target_plus.jpg':
        
            #setting the key press that will be the correct answer
            corrAns = 'b'
        target_image.setPos((position_trial, 0))
        target_image.setImage(TargetImage)
        keyboard_response.keys = []
        keyboard_response.rt = []
        _keyboard_response_allKeys = []
        # keep track of which components have finished
        trialComponents = [practice_trial_square, practice_trial_plus, practice_trial_cross, left_far_tile, left_mid_tile, right_mid_tile, right_far_tile, target_image, keyboard_response]
        for thisComponent in trialComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "trial" ---
        routineForceEnded = not continueRoutine
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *practice_trial_square* updates
            
            # if practice_trial_square is starting this frame...
            if practice_trial_square.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                practice_trial_square.frameNStart = frameN  # exact frame index
                practice_trial_square.tStart = t  # local t and not account for scr refresh
                practice_trial_square.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(practice_trial_square, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'practice_trial_square.started')
                # update status
                practice_trial_square.status = STARTED
                practice_trial_square.setAutoDraw(True)
            
            # if practice_trial_square is active this frame...
            if practice_trial_square.status == STARTED:
                # update params
                pass
            
            # *practice_trial_plus* updates
            
            # if practice_trial_plus is starting this frame...
            if practice_trial_plus.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                practice_trial_plus.frameNStart = frameN  # exact frame index
                practice_trial_plus.tStart = t  # local t and not account for scr refresh
                practice_trial_plus.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(practice_trial_plus, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'practice_trial_plus.started')
                # update status
                practice_trial_plus.status = STARTED
                practice_trial_plus.setAutoDraw(True)
            
            # if practice_trial_plus is active this frame...
            if practice_trial_plus.status == STARTED:
                # update params
                pass
            
            # *practice_trial_cross* updates
            
            # if practice_trial_cross is starting this frame...
            if practice_trial_cross.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                practice_trial_cross.frameNStart = frameN  # exact frame index
                practice_trial_cross.tStart = t  # local t and not account for scr refresh
                practice_trial_cross.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(practice_trial_cross, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'practice_trial_cross.started')
                # update status
                practice_trial_cross.status = STARTED
                practice_trial_cross.setAutoDraw(True)
            
            # if practice_trial_cross is active this frame...
            if practice_trial_cross.status == STARTED:
                # update params
                pass
            
            # *left_far_tile* updates
            
            # if left_far_tile is starting this frame...
            if left_far_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                left_far_tile.frameNStart = frameN  # exact frame index
                left_far_tile.tStart = t  # local t and not account for scr refresh
                left_far_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(left_far_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'left_far_tile.started')
                # update status
                left_far_tile.status = STARTED
                left_far_tile.setAutoDraw(True)
            
            # if left_far_tile is active this frame...
            if left_far_tile.status == STARTED:
                # update params
                pass
            
            # *left_mid_tile* updates
            
            # if left_mid_tile is starting this frame...
            if left_mid_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                left_mid_tile.frameNStart = frameN  # exact frame index
                left_mid_tile.tStart = t  # local t and not account for scr refresh
                left_mid_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(left_mid_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'left_mid_tile.started')
                # update status
                left_mid_tile.status = STARTED
                left_mid_tile.setAutoDraw(True)
            
            # if left_mid_tile is active this frame...
            if left_mid_tile.status == STARTED:
                # update params
                pass
            
            # *right_mid_tile* updates
            
            # if right_mid_tile is starting this frame...
            if right_mid_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                right_mid_tile.frameNStart = frameN  # exact frame index
                right_mid_tile.tStart = t  # local t and not account for scr refresh
                right_mid_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(right_mid_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'right_mid_tile.started')
                # update status
                right_mid_tile.status = STARTED
                right_mid_tile.setAutoDraw(True)
            
            # if right_mid_tile is active this frame...
            if right_mid_tile.status == STARTED:
                # update params
                pass
            
            # *right_far_tile* updates
            
            # if right_far_tile is starting this frame...
            if right_far_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                right_far_tile.frameNStart = frameN  # exact frame index
                right_far_tile.tStart = t  # local t and not account for scr refresh
                right_far_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(right_far_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'right_far_tile.started')
                # update status
                right_far_tile.status = STARTED
                right_far_tile.setAutoDraw(True)
            
            # if right_far_tile is active this frame...
            if right_far_tile.status == STARTED:
                # update params
                pass
            
            # *target_image* updates
            
            # if target_image is starting this frame...
            if target_image.status == NOT_STARTED and tThisFlip >= onset_trial-frameTolerance:
                # keep track of start time/frame for later
                target_image.frameNStart = frameN  # exact frame index
                target_image.tStart = t  # local t and not account for scr refresh
                target_image.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(target_image, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'target_image.started')
                # update status
                target_image.status = STARTED
                target_image.setAutoDraw(True)
            
            # if target_image is active this frame...
            if target_image.status == STARTED:
                # update params
                pass
            
            # if target_image is stopping this frame...
            if target_image.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > target_image.tStartRefresh + 0.2-frameTolerance:
                    # keep track of stop time/frame for later
                    target_image.tStop = t  # not accounting for scr refresh
                    target_image.frameNStop = frameN  # exact frame index
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_image.stopped')
                    # update status
                    target_image.status = FINISHED
                    target_image.setAutoDraw(False)
            
            # *keyboard_response* updates
            
            # if keyboard_response is starting this frame...
            if keyboard_response.status == NOT_STARTED and t >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                keyboard_response.frameNStart = frameN  # exact frame index
                keyboard_response.tStart = t  # local t and not account for scr refresh
                keyboard_response.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(keyboard_response, 'tStartRefresh')  # time at next scr refresh
                # update status
                keyboard_response.status = STARTED
                # keyboard checking is just starting
                keyboard_response.clock.reset()  # now t=0
                keyboard_response.clearEvents(eventType='keyboard')
            if keyboard_response.status == STARTED:
                theseKeys = keyboard_response.getKeys(keyList=['c', 'v', 'b'], ignoreKeys=["escape"], waitRelease=False)
                _keyboard_response_allKeys.extend(theseKeys)
                if len(_keyboard_response_allKeys):
                    keyboard_response.keys = _keyboard_response_allKeys[-1].name  # just the last key pressed
                    keyboard_response.rt = _keyboard_response_allKeys[-1].rt
                    keyboard_response.duration = _keyboard_response_allKeys[-1].duration
                    # was this correct?
                    if (keyboard_response.keys == str(corrAns)) or (keyboard_response.keys == corrAns):
                        keyboard_response.corr = 1
                    else:
                        keyboard_response.corr = 0
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if defaultKeyboard.getKeys(keyList=["escape"]):
                thisExp.status = FINISHED
            if thisExp.status == FINISHED or endExpNow:
                endExperiment(thisExp, inputs=inputs, win=win)
                return
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in trialComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "trial" ---
        for thisComponent in trialComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        thisExp.addData('trial.stopped', globalClock.getTime())
        # Run 'End Routine' code from compt_RT_acc
        #this code is to record the reaction times and accuracy of the trial
        
        thisRoutineDuration = t # how long did this trial last
        
        # keyboard_response.rt is the time with which a key was pressed
        # thisRecRT - how long it took for participants to respond after onset of target_image
        # thisAcc - whether or not the response was correct
        
        # compute RT based on onset of target
        thisRecRT = keyboard_response.rt - onset_trial 
        
        # check of the response was correct
        if keyboard_response.corr == 1: 
        
            # if it was correct, assign 'correct' value
            thisAcc = 'correct' 
        
        # if not, assign 'incorrect' value
        else:
            thisAcc = 'incorrect'
        
        # record the actual response times of each trial 
        thisExp.addData('trialRespTimes', thisRecRT) 
        
        # check responses
        if keyboard_response.keys in ['', [], None]:  # No response was made
            keyboard_response.keys = None
            # was no response the correct answer?!
            if str(corrAns).lower() == 'none':
               keyboard_response.corr = 1;  # correct non-response
            else:
               keyboard_response.corr = 0;  # failed to respond (incorrectly)
        # store data for PracticeLoop (TrialHandler)
        PracticeLoop.addData('keyboard_response.keys',keyboard_response.keys)
        PracticeLoop.addData('keyboard_response.corr', keyboard_response.corr)
        if keyboard_response.keys != None:  # we had a response
            PracticeLoop.addData('keyboard_response.rt', keyboard_response.rt)
            PracticeLoop.addData('keyboard_response.duration', keyboard_response.duration)
        # the Routine "trial" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        
        # --- Prepare to start Routine "practice_feedback" ---
        continueRoutine = True
        # update component parameters for each repeat
        thisExp.addData('practice_feedback.started', globalClock.getTime())
        feedback_text.setText("The last recorded RT was: "  + str(round(thisRecRT, 3)) + " \nThe response was: "  + thisAcc + " \n\nPress the space bar to continue."  
        )
        next_trial.keys = []
        next_trial.rt = []
        _next_trial_allKeys = []
        # keep track of which components have finished
        practice_feedbackComponents = [feedback_text, next_trial]
        for thisComponent in practice_feedbackComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "practice_feedback" ---
        routineForceEnded = not continueRoutine
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *feedback_text* updates
            
            # if feedback_text is starting this frame...
            if feedback_text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                feedback_text.frameNStart = frameN  # exact frame index
                feedback_text.tStart = t  # local t and not account for scr refresh
                feedback_text.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(feedback_text, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'feedback_text.started')
                # update status
                feedback_text.status = STARTED
                feedback_text.setAutoDraw(True)
            
            # if feedback_text is active this frame...
            if feedback_text.status == STARTED:
                # update params
                pass
            
            # *next_trial* updates
            waitOnFlip = False
            
            # if next_trial is starting this frame...
            if next_trial.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                next_trial.frameNStart = frameN  # exact frame index
                next_trial.tStart = t  # local t and not account for scr refresh
                next_trial.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(next_trial, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'next_trial.started')
                # update status
                next_trial.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(next_trial.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(next_trial.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if next_trial.status == STARTED and not waitOnFlip:
                theseKeys = next_trial.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
                _next_trial_allKeys.extend(theseKeys)
                if len(_next_trial_allKeys):
                    next_trial.keys = _next_trial_allKeys[-1].name  # just the last key pressed
                    next_trial.rt = _next_trial_allKeys[-1].rt
                    next_trial.duration = _next_trial_allKeys[-1].duration
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if defaultKeyboard.getKeys(keyList=["escape"]):
                thisExp.status = FINISHED
            if thisExp.status == FINISHED or endExpNow:
                endExperiment(thisExp, inputs=inputs, win=win)
                return
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in practice_feedbackComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "practice_feedback" ---
        for thisComponent in practice_feedbackComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        thisExp.addData('practice_feedback.stopped', globalClock.getTime())
        # check responses
        if next_trial.keys in ['', [], None]:  # No response was made
            next_trial.keys = None
        PracticeLoop.addData('next_trial.keys',next_trial.keys)
        if next_trial.keys != None:  # we had a response
            PracticeLoop.addData('next_trial.rt', next_trial.rt)
            PracticeLoop.addData('next_trial.duration', next_trial.duration)
        # the Routine "practice_feedback" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        thisExp.nextEntry()
        
        if thisSession is not None:
            # if running in a Session with a Liaison client, send data up to now
            thisSession.sendExperimentData()
    # completed 1.0 repeats of 'PracticeLoop'
    
    # get names of stimulus parameters
    if PracticeLoop.trialList in ([], [None], None):
        params = []
    else:
        params = PracticeLoop.trialList[0].keys()
    # save data for this loop
    PracticeLoop.saveAsText(filename + 'PracticeLoop.csv', delim=',',
        stimOut=params,
        dataOut=['n','all_mean','all_std', 'all_raw'])
    
    # --- Prepare to start Routine "instructions_shape_exp" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('instructions_shape_exp.started', globalClock.getTime())
    crtt_space_2.keys = []
    crtt_space_2.rt = []
    _crtt_space_2_allKeys = []
    # keep track of which components have finished
    instructions_shape_expComponents = [crtt_instr_msg_2, crtt_space_2]
    for thisComponent in instructions_shape_expComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "instructions_shape_exp" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *crtt_instr_msg_2* updates
        
        # if crtt_instr_msg_2 is starting this frame...
        if crtt_instr_msg_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            crtt_instr_msg_2.frameNStart = frameN  # exact frame index
            crtt_instr_msg_2.tStart = t  # local t and not account for scr refresh
            crtt_instr_msg_2.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(crtt_instr_msg_2, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'crtt_instr_msg_2.started')
            # update status
            crtt_instr_msg_2.status = STARTED
            crtt_instr_msg_2.setAutoDraw(True)
        
        # if crtt_instr_msg_2 is active this frame...
        if crtt_instr_msg_2.status == STARTED:
            # update params
            pass
        
        # *crtt_space_2* updates
        waitOnFlip = False
        
        # if crtt_space_2 is starting this frame...
        if crtt_space_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            crtt_space_2.frameNStart = frameN  # exact frame index
            crtt_space_2.tStart = t  # local t and not account for scr refresh
            crtt_space_2.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(crtt_space_2, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'crtt_space_2.started')
            # update status
            crtt_space_2.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(crtt_space_2.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(crtt_space_2.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if crtt_space_2.status == STARTED and not waitOnFlip:
            theseKeys = crtt_space_2.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _crtt_space_2_allKeys.extend(theseKeys)
            if len(_crtt_space_2_allKeys):
                crtt_space_2.keys = _crtt_space_2_allKeys[-1].name  # just the last key pressed
                crtt_space_2.rt = _crtt_space_2_allKeys[-1].rt
                crtt_space_2.duration = _crtt_space_2_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in instructions_shape_expComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "instructions_shape_exp" ---
    for thisComponent in instructions_shape_expComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('instructions_shape_exp.stopped', globalClock.getTime())
    # check responses
    if crtt_space_2.keys in ['', [], None]:  # No response was made
        crtt_space_2.keys = None
    thisExp.addData('crtt_space_2.keys',crtt_space_2.keys)
    if crtt_space_2.keys != None:  # we had a response
        thisExp.addData('crtt_space_2.rt', crtt_space_2.rt)
        thisExp.addData('crtt_space_2.duration', crtt_space_2.duration)
    thisExp.nextEntry()
    # the Routine "instructions_shape_exp" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # set up handler to look after randomisation of conditions etc
    expLoop = data.TrialHandler(nReps=2.0, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=data.importConditions('/Users/philippflieger/PhD/NOWA/choice_rtt/stimuli/shapes/shapes_targets.csv'),
        seed=None, name='expLoop')
    thisExp.addLoop(expLoop)  # add the loop to the experiment
    thisExpLoop = expLoop.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisExpLoop.rgb)
    if thisExpLoop != None:
        for paramName in thisExpLoop:
            globals()[paramName] = thisExpLoop[paramName]
    
    for thisExpLoop in expLoop:
        currentLoop = expLoop
        thisExp.timestampOnFlip(win, 'thisRow.t')
        # pause experiment here if requested
        if thisExp.status == PAUSED:
            pauseExperiment(
                thisExp=thisExp, 
                inputs=inputs, 
                win=win, 
                timers=[routineTimer], 
                playbackComponents=[]
        )
        # abbreviate parameter names if possible (e.g. rgb = thisExpLoop.rgb)
        if thisExpLoop != None:
            for paramName in thisExpLoop:
                globals()[paramName] = thisExpLoop[paramName]
        
        # --- Prepare to start Routine "trial" ---
        continueRoutine = True
        # update component parameters for each repeat
        thisExp.addData('trial.started', globalClock.getTime())
        # Run 'Begin Routine' code from jitter_onset_loc
        #list of possible onsets for target
        list_onsets = [1, 1.2, 1.4, 1.6, 1.8]
        
        # randomize these onsets
        shuffle(list_onsets)
        
        #pick the first value from the list 
        onset_trial = list_onsets[0]
        
        #list of possible positions for target
        list_positions = [-0.375, -0.125, 0.125, 0.375]
        
        # randomize these onsets
        shuffle(list_positions)
        
        #pick the first value from the list 
        position_trial = list_positions[0]
        
        #TargetImage is the variable in the Image field of the target_image component
        
        #path of where to find the target image
        if TargetImage == '../../stimuli/shapes/target_square.jpg': #path of where to find the target image
            
            #setting the key press that will be the correct answer
            corrAns = 'v' 
            
        #path of where to find the target image
        elif TargetImage == '../../stimuli/shapes/target_cross.jpg':
        
            #setting the key press that will be the correct answer
            corrAns = 'c'
        
        #path of where to find the target image
        elif TargetImage == '../../stimuli/shapes/target_plus.jpg':
        
            #setting the key press that will be the correct answer
            corrAns = 'b'
        target_image.setPos((position_trial, 0))
        target_image.setImage(TargetImage)
        keyboard_response.keys = []
        keyboard_response.rt = []
        _keyboard_response_allKeys = []
        # keep track of which components have finished
        trialComponents = [practice_trial_square, practice_trial_plus, practice_trial_cross, left_far_tile, left_mid_tile, right_mid_tile, right_far_tile, target_image, keyboard_response]
        for thisComponent in trialComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "trial" ---
        routineForceEnded = not continueRoutine
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *practice_trial_square* updates
            
            # if practice_trial_square is starting this frame...
            if practice_trial_square.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                practice_trial_square.frameNStart = frameN  # exact frame index
                practice_trial_square.tStart = t  # local t and not account for scr refresh
                practice_trial_square.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(practice_trial_square, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'practice_trial_square.started')
                # update status
                practice_trial_square.status = STARTED
                practice_trial_square.setAutoDraw(True)
            
            # if practice_trial_square is active this frame...
            if practice_trial_square.status == STARTED:
                # update params
                pass
            
            # *practice_trial_plus* updates
            
            # if practice_trial_plus is starting this frame...
            if practice_trial_plus.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                practice_trial_plus.frameNStart = frameN  # exact frame index
                practice_trial_plus.tStart = t  # local t and not account for scr refresh
                practice_trial_plus.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(practice_trial_plus, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'practice_trial_plus.started')
                # update status
                practice_trial_plus.status = STARTED
                practice_trial_plus.setAutoDraw(True)
            
            # if practice_trial_plus is active this frame...
            if practice_trial_plus.status == STARTED:
                # update params
                pass
            
            # *practice_trial_cross* updates
            
            # if practice_trial_cross is starting this frame...
            if practice_trial_cross.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                practice_trial_cross.frameNStart = frameN  # exact frame index
                practice_trial_cross.tStart = t  # local t and not account for scr refresh
                practice_trial_cross.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(practice_trial_cross, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'practice_trial_cross.started')
                # update status
                practice_trial_cross.status = STARTED
                practice_trial_cross.setAutoDraw(True)
            
            # if practice_trial_cross is active this frame...
            if practice_trial_cross.status == STARTED:
                # update params
                pass
            
            # *left_far_tile* updates
            
            # if left_far_tile is starting this frame...
            if left_far_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                left_far_tile.frameNStart = frameN  # exact frame index
                left_far_tile.tStart = t  # local t and not account for scr refresh
                left_far_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(left_far_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'left_far_tile.started')
                # update status
                left_far_tile.status = STARTED
                left_far_tile.setAutoDraw(True)
            
            # if left_far_tile is active this frame...
            if left_far_tile.status == STARTED:
                # update params
                pass
            
            # *left_mid_tile* updates
            
            # if left_mid_tile is starting this frame...
            if left_mid_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                left_mid_tile.frameNStart = frameN  # exact frame index
                left_mid_tile.tStart = t  # local t and not account for scr refresh
                left_mid_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(left_mid_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'left_mid_tile.started')
                # update status
                left_mid_tile.status = STARTED
                left_mid_tile.setAutoDraw(True)
            
            # if left_mid_tile is active this frame...
            if left_mid_tile.status == STARTED:
                # update params
                pass
            
            # *right_mid_tile* updates
            
            # if right_mid_tile is starting this frame...
            if right_mid_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                right_mid_tile.frameNStart = frameN  # exact frame index
                right_mid_tile.tStart = t  # local t and not account for scr refresh
                right_mid_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(right_mid_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'right_mid_tile.started')
                # update status
                right_mid_tile.status = STARTED
                right_mid_tile.setAutoDraw(True)
            
            # if right_mid_tile is active this frame...
            if right_mid_tile.status == STARTED:
                # update params
                pass
            
            # *right_far_tile* updates
            
            # if right_far_tile is starting this frame...
            if right_far_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                right_far_tile.frameNStart = frameN  # exact frame index
                right_far_tile.tStart = t  # local t and not account for scr refresh
                right_far_tile.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(right_far_tile, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'right_far_tile.started')
                # update status
                right_far_tile.status = STARTED
                right_far_tile.setAutoDraw(True)
            
            # if right_far_tile is active this frame...
            if right_far_tile.status == STARTED:
                # update params
                pass
            
            # *target_image* updates
            
            # if target_image is starting this frame...
            if target_image.status == NOT_STARTED and tThisFlip >= onset_trial-frameTolerance:
                # keep track of start time/frame for later
                target_image.frameNStart = frameN  # exact frame index
                target_image.tStart = t  # local t and not account for scr refresh
                target_image.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(target_image, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'target_image.started')
                # update status
                target_image.status = STARTED
                target_image.setAutoDraw(True)
            
            # if target_image is active this frame...
            if target_image.status == STARTED:
                # update params
                pass
            
            # if target_image is stopping this frame...
            if target_image.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > target_image.tStartRefresh + 0.2-frameTolerance:
                    # keep track of stop time/frame for later
                    target_image.tStop = t  # not accounting for scr refresh
                    target_image.frameNStop = frameN  # exact frame index
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_image.stopped')
                    # update status
                    target_image.status = FINISHED
                    target_image.setAutoDraw(False)
            
            # *keyboard_response* updates
            
            # if keyboard_response is starting this frame...
            if keyboard_response.status == NOT_STARTED and t >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                keyboard_response.frameNStart = frameN  # exact frame index
                keyboard_response.tStart = t  # local t and not account for scr refresh
                keyboard_response.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(keyboard_response, 'tStartRefresh')  # time at next scr refresh
                # update status
                keyboard_response.status = STARTED
                # keyboard checking is just starting
                keyboard_response.clock.reset()  # now t=0
                keyboard_response.clearEvents(eventType='keyboard')
            if keyboard_response.status == STARTED:
                theseKeys = keyboard_response.getKeys(keyList=['c', 'v', 'b'], ignoreKeys=["escape"], waitRelease=False)
                _keyboard_response_allKeys.extend(theseKeys)
                if len(_keyboard_response_allKeys):
                    keyboard_response.keys = _keyboard_response_allKeys[-1].name  # just the last key pressed
                    keyboard_response.rt = _keyboard_response_allKeys[-1].rt
                    keyboard_response.duration = _keyboard_response_allKeys[-1].duration
                    # was this correct?
                    if (keyboard_response.keys == str(corrAns)) or (keyboard_response.keys == corrAns):
                        keyboard_response.corr = 1
                    else:
                        keyboard_response.corr = 0
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if defaultKeyboard.getKeys(keyList=["escape"]):
                thisExp.status = FINISHED
            if thisExp.status == FINISHED or endExpNow:
                endExperiment(thisExp, inputs=inputs, win=win)
                return
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in trialComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "trial" ---
        for thisComponent in trialComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        thisExp.addData('trial.stopped', globalClock.getTime())
        # Run 'End Routine' code from compt_RT_acc
        #this code is to record the reaction times and accuracy of the trial
        
        thisRoutineDuration = t # how long did this trial last
        
        # keyboard_response.rt is the time with which a key was pressed
        # thisRecRT - how long it took for participants to respond after onset of target_image
        # thisAcc - whether or not the response was correct
        
        # compute RT based on onset of target
        thisRecRT = keyboard_response.rt - onset_trial 
        
        # check of the response was correct
        if keyboard_response.corr == 1: 
        
            # if it was correct, assign 'correct' value
            thisAcc = 'correct' 
        
        # if not, assign 'incorrect' value
        else:
            thisAcc = 'incorrect'
        
        # record the actual response times of each trial 
        thisExp.addData('trialRespTimes', thisRecRT) 
        
        # check responses
        if keyboard_response.keys in ['', [], None]:  # No response was made
            keyboard_response.keys = None
            # was no response the correct answer?!
            if str(corrAns).lower() == 'none':
               keyboard_response.corr = 1;  # correct non-response
            else:
               keyboard_response.corr = 0;  # failed to respond (incorrectly)
        # store data for expLoop (TrialHandler)
        expLoop.addData('keyboard_response.keys',keyboard_response.keys)
        expLoop.addData('keyboard_response.corr', keyboard_response.corr)
        if keyboard_response.keys != None:  # we had a response
            expLoop.addData('keyboard_response.rt', keyboard_response.rt)
            expLoop.addData('keyboard_response.duration', keyboard_response.duration)
        # the Routine "trial" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        thisExp.nextEntry()
        
        if thisSession is not None:
            # if running in a Session with a Liaison client, send data up to now
            thisSession.sendExperimentData()
    # completed 2.0 repeats of 'expLoop'
    
    # get names of stimulus parameters
    if expLoop.trialList in ([], [None], None):
        params = []
    else:
        params = expLoop.trialList[0].keys()
    # save data for this loop
    expLoop.saveAsText(filename + 'expLoop.csv', delim=',',
        stimOut=params,
        dataOut=['n','all_mean','all_std', 'all_raw'])
    
    # --- Prepare to start Routine "end_screen" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('end_screen.started', globalClock.getTime())
    # keep track of which components have finished
    end_screenComponents = [end_screen_msg]
    for thisComponent in end_screenComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "end_screen" ---
    routineForceEnded = not continueRoutine
    while continueRoutine and routineTimer.getTime() < 1.0:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *end_screen_msg* updates
        
        # if end_screen_msg is starting this frame...
        if end_screen_msg.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            end_screen_msg.frameNStart = frameN  # exact frame index
            end_screen_msg.tStart = t  # local t and not account for scr refresh
            end_screen_msg.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(end_screen_msg, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'end_screen_msg.started')
            # update status
            end_screen_msg.status = STARTED
            end_screen_msg.setAutoDraw(True)
        
        # if end_screen_msg is active this frame...
        if end_screen_msg.status == STARTED:
            # update params
            pass
        
        # if end_screen_msg is stopping this frame...
        if end_screen_msg.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > end_screen_msg.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                end_screen_msg.tStop = t  # not accounting for scr refresh
                end_screen_msg.frameNStop = frameN  # exact frame index
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'end_screen_msg.stopped')
                # update status
                end_screen_msg.status = FINISHED
                end_screen_msg.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in end_screenComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "end_screen" ---
    for thisComponent in end_screenComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('end_screen.stopped', globalClock.getTime())
    # using non-slip timing so subtract the expected duration of this Routine (unless ended on request)
    if routineForceEnded:
        routineTimer.reset()
    else:
        routineTimer.addTime(-1.000000)
    
    # mark experiment as finished
    endExperiment(thisExp, win=win, inputs=inputs)


def saveData(thisExp):
    """
    Save data from this experiment
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    """
    filename = thisExp.dataFileName
    # these shouldn't be strictly necessary (should auto-save)
    thisExp.saveAsPickle(filename)


def endExperiment(thisExp, inputs=None, win=None):
    """
    End this experiment, performing final shut down operations.
    
    This function does NOT close the window or end the Python process - use `quit` for this.
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    inputs : dict
        Dictionary of input devices by name.
    win : psychopy.visual.Window
        Window for this experiment.
    """
    if win is not None:
        # remove autodraw from all current components
        win.clearAutoDraw()
        # Flip one final time so any remaining win.callOnFlip() 
        # and win.timeOnFlip() tasks get executed
        win.flip()
    # mark experiment handler as finished
    thisExp.status = FINISHED
    # shut down eyetracker, if there is one
    if inputs is not None:
        if 'eyetracker' in inputs and inputs['eyetracker'] is not None:
            inputs['eyetracker'].setConnectionState(False)
    logging.flush()


def quit(thisExp, win=None, inputs=None, thisSession=None):
    """
    Fully quit, closing the window and ending the Python process.
    
    Parameters
    ==========
    win : psychopy.visual.Window
        Window to close.
    inputs : dict
        Dictionary of input devices by name.
    thisSession : psychopy.session.Session or None
        Handle of the Session object this experiment is being run from, if any.
    """
    thisExp.abort()  # or data files will save again on exit
    # make sure everything is closed down
    if win is not None:
        # Flip one final time so any remaining win.callOnFlip() 
        # and win.timeOnFlip() tasks get executed before quitting
        win.flip()
        win.close()
    if inputs is not None:
        if 'eyetracker' in inputs and inputs['eyetracker'] is not None:
            inputs['eyetracker'].setConnectionState(False)
    logging.flush()
    if thisSession is not None:
        thisSession.stop()
    # terminate Python process
    core.quit()


# if running this experiment as a script...
if __name__ == '__main__':
    # call all functions in order
    expInfo = showExpInfoDlg(expInfo=expInfo)
    thisExp = setupData(expInfo=expInfo)
    logFile = setupLogging(filename=thisExp.dataFileName)
    win = setupWindow(expInfo=expInfo)
    inputs = setupInputs(expInfo=expInfo, thisExp=thisExp, win=win)
    run(
        expInfo=expInfo, 
        thisExp=thisExp, 
        win=win, 
        inputs=inputs
    )
    saveData(thisExp=thisExp)
    quit(thisExp=thisExp, win=win, inputs=inputs)
